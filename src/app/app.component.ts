import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private holder: String  = "truc a faire";
  public todos: any[] = [];
  public describe: String = this.holder;


  add(): void{
    this.todos.push({
      describe : this.describe
    });
    this.describe = this.holder;
  };

}
